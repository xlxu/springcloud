package com.chigo.cloud.controller;

import com.chigo.cloud.entities.CommonResult;
import com.chigo.cloud.entities.Payment;
import com.chigo.cloud.service.PaymentService;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.TimeUnit;

/**
 * @auther cyxu
 * @date 2023-03-25 17:10
 */
@Slf4j
@RestController
@RequestMapping("/payment")
public class PaymentController {

    @Resource
    private PaymentService paymentService;

    @Value("${server.port}")
    private String serverPort;

    @PostMapping(value = "/add")
    public CommonResult addPayment(@RequestBody Payment payment) {
        int result = paymentService.addPayment(payment);
        log.info("serverPort:{}", serverPort);
        if (result > 0) {
            return new CommonResult(200, "操作成功！serverPort:" + serverPort, result);
        }
        return new CommonResult(201, "操作失败！serverPort:" + serverPort, null);
    }

    @GetMapping(value = "/findPayment/{id}")
    public CommonResult getPaymentById(@PathVariable("id") Long id) {
        var payment = paymentService.getPaymentById(id);
        log.info("serverPort:{}", serverPort);
        if (null != payment) {
            return new CommonResult(200, "操作成功！serverPort:" + serverPort, payment);
        }
        return new CommonResult(201, "未查询到数据！serverPort:" + serverPort, null);
    }

    @GetMapping("/feign/timeOut")
    public String feignTimeOut() {
        try {
            //暂停几秒
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return serverPort;
    }

}
