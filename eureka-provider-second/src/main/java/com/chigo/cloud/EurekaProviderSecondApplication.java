package com.chigo.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
/**
 * @auther cyxu
 * @date 2023-03-23 21:56
 */
@SpringBootApplication
@EnableDiscoveryClient
public class EurekaProviderSecondApplication {
    public static void main(String[] args) {
        SpringApplication.run(EurekaProviderSecondApplication.class, args);
    }
}