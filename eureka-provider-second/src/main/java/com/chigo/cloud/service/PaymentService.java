package com.chigo.cloud.service;

import com.chigo.cloud.entities.Payment;

/**
 * TODO
 *
 * @auther cyxu
 * @date 2023-03-25 17:06
 */
public interface PaymentService {
    int addPayment(Payment payment);

    Payment getPaymentById(Long id);
}
