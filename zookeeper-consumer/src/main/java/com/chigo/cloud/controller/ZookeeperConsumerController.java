package com.chigo.cloud.controller;

import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @auther cyxu
 * @date 2023-03-25 21:03
 */
@Slf4j
@RestController
@RequestMapping("/consumer")
public class ZookeeperConsumerController {

    private static final String PROVIDER_URL = "http://zookeeper-provider/chigo/provider";//

    @Resource
    private RestTemplate restTemplate;


    @GetMapping("/zk")
    public String zkinfo() {
        return restTemplate.getForObject(PROVIDER_URL + "/zk", String.class);
    }

}
