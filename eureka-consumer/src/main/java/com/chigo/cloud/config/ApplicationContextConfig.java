package com.chigo.cloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @auther cyxu
 * @date 2023-03-25 21:10
 */
@Configuration
public class ApplicationContextConfig {

    @Bean
    @LoadBalanced  //自定义轮询规则时不需要
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

}
