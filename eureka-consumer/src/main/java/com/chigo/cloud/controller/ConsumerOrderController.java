package com.chigo.cloud.controller;

import com.chigo.cloud.entities.CommonResult;
import com.chigo.cloud.entities.Payment;

import javax.annotation.Resource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


/**
 * TODO
 *
 * @auther cyxu
 * @date 2023-03-25 21:03
 */
@Slf4j
@RestController
@RequestMapping("/consumer")
public class ConsumerOrderController {

    //        private static final String PAYMENT_URL = "http://127.0.0.1:8101/chigo/payment";//单机版
    private static final String PAYMENT_URL = "http://PROVIDER-SERVICE/chigo/payment";//集群版 微服务名称

    @Resource
    private RestTemplate restTemplate;

    @GetMapping("/payment/add")
    public CommonResult<Payment> addPayment(Payment payment) {
        return restTemplate.postForObject(PAYMENT_URL + "/add", payment, CommonResult.class);
    }

    @GetMapping(value = "/findPayment/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id) {
        return restTemplate.getForObject(PAYMENT_URL + "/findPayment/" + id, CommonResult.class);
    }

}
