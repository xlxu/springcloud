package com.chigo.cloud;


import com.chigo.ribbon.RibbonRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;


/**
 * @auther cyxu
 * @date 2023-03-25 20:55
 */
@SpringBootApplication
@EnableDiscoveryClient
//@RibbonClient(name = "PROVIDER-SERVICE", configuration = RibbonRule.class)
public class EurekaConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(EurekaConsumerApplication.class, args);
    }
}