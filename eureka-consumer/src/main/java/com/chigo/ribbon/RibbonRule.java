package com.chigo.ribbon;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自定义ribbon替换规则配置文件，不能放在@ComponentScan注解同包及子包下面
 *
 * @auther cyxu
 * @date 2023-04-05 20:38
 */
@Configuration
public class RibbonRule {
    @Bean
    public IRule myRule(){
        return new RandomRule();// 改为我们自定义的算法，每个服务执行5次
    }

}
