package com.chigo.cloud.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @auther cyxu
 * @date 2023-04-05 16:52
 */
@Slf4j
@RestController
@RequestMapping("/provider")
public class providerController {

    @Value("${server.port}")
    private String serverPort;

    @RequestMapping("/consul")
    public String providerConsul() {
        return "serverPort: " + serverPort + " " + System.currentTimeMillis();
    }
}
