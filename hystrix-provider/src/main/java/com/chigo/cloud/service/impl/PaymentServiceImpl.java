package com.chigo.cloud.service.impl;

import com.chigo.cloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @auther cyxu
 * @date 2023-03-25 17:07
 */
@Slf4j
@Service
public class PaymentServiceImpl implements PaymentService {


    @Override
    public String paymentInfOk() {
        return "线程：" + Thread.currentThread().getName() + " paymentInfOk";
    }

    @Override
    public String paymentInfoTimeOut() {
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            log.error(e.getMessage());
        }
        return "线程：" + Thread.currentThread().getName() + " paymentInfoTimeOut";
    }
}
