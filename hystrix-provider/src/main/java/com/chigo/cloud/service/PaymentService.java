package com.chigo.cloud.service;

import com.chigo.cloud.entities.Payment;

/**
 * @auther cyxu
 * @date 2023-03-25 17:06
 */
public interface PaymentService {
    String paymentInfOk();

    String paymentInfoTimeOut();

}
