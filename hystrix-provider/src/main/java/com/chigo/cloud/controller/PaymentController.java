package com.chigo.cloud.controller;

import com.chigo.cloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @auther cyxu
 * @date 2023-03-25 17:10
 */
@Slf4j
@RestController
@RequestMapping("/payment/hystrix")
public class PaymentController {

    @Resource
    private PaymentService paymentService;

    @Value("${server.port}")
    private String serverPort;

    @GetMapping("/ok")
    public String paymentInfOk() {
        String result = paymentService.paymentInfOk();
        log.info(result);
        return result;
    }

    @GetMapping("/timeout")
    public String paymentInfoTimeOut() {
        String result = paymentService.paymentInfoTimeOut();
        log.info(result);
        return result;
    }
}
