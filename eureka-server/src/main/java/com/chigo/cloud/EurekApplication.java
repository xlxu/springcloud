package com.chigo.cloud;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 *
 * @auther cyxu
 * @date 2023-03-26 10:44
 */
@Slf4j
@SpringBootApplication
@EnableEurekaServer
public class EurekApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekApplication.class, args);
    }
}