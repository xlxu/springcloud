package com.chigo.cloud.controller;

import com.chigo.cloud.entities.CommonResult;
import com.chigo.cloud.entities.Payment;
import com.chigo.cloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @auther cyxu
 * @date 2023-03-25 17:10
 */
@Slf4j
@RestController
@RequestMapping("/payment")
public class PaymentController {

    @Resource
    private PaymentService paymentService;

    @PostMapping(value = "/add")
    public CommonResult addPayment(@RequestBody Payment payment) {
        int result = paymentService.addPayment(payment);
        if (result > 0) {
            return new CommonResult(200, "操作成功！", result);
        }
        return new CommonResult(201, "操作失败！", null);
    }

    @GetMapping(value = "/findPayment/{id}")
    public CommonResult getPaymentById(@PathVariable("id") Long id) {
        var payment = paymentService.getPaymentById(id);
        if (null != payment) {
            return new CommonResult(200, "操作成功！", payment);
        }
        return new CommonResult(201, "未查询到数据！", null);
    }

}
