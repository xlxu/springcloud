package com.chigo.cloud.dao;

import com.chigo.cloud.entities.Payment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @auther cyxu
 * @date 2023-03-25 16:42
 */
@Mapper
public interface PaymentDao {

    int addPayment(Payment payment);

    Payment getPaymentById(@Param("id") Long id);

}
