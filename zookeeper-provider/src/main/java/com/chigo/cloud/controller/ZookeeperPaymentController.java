package com.chigo.cloud.controller;

import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @auther cyxu
 * @date 2023-03-25 17:10
 */
@Slf4j
@RestController
@RequestMapping("/provider")
public class ZookeeperPaymentController {

    @Value("${server.port}")
    private String serverPort;

    @Resource
    private DiscoveryClient discoveryClient;

    @RequestMapping("/zk")
    public String paymentZk() {
        return "serverPort: " + serverPort;
    }

}
