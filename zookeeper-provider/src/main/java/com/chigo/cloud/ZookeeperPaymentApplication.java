package com.chigo.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Auther cyxu
 * @date 2023-04-01 19:05
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ZookeeperPaymentApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZookeeperPaymentApplication.class, args);
    }
}