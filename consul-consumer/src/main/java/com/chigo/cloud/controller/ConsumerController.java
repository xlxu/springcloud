package com.chigo.cloud.controller;

import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * TODO
 *
 * @auther cyxu
 * @date 2023-03-25 21:03
 */
@Slf4j
@RestController
@RequestMapping("/consumer")
public class ConsumerController {

    private static final String PROVIDER_URL = "http://consul-provider/chigo/provider";//

    @Resource
    private RestTemplate restTemplate;


    @GetMapping("/consul")
    public String consulInfo() {
        return restTemplate.getForObject(PROVIDER_URL + "/consul", String.class);
    }

}
