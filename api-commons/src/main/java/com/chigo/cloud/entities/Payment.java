package com.chigo.cloud.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * TODO
 *
 * @auther cyxu
 * @date 2023-03-25 16:35
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Payment implements Serializable {

    /**
     * 主键
     */
    private Long id;
    /**
     * 序列号
     */
    private String serial;
}
