package com.chigo.cloud.service;

import com.chigo.cloud.entities.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * openfeign 注解接口
 *
 * @auther cyxu
 * @date 2023-04-13 20:52
 */
@Component
@FeignClient(value = "PROVIDER-SERVICE")//微服务名称
public interface IPaymentService {

    /**
     * 调用的接口地址
     *
     * @param id
     * @return CommonResult
     * @author cyxu
     * @date 2023-04-13 21:37
     */
    @GetMapping(value = "/chigo/payment/findPayment/{id}")
    CommonResult getPaymentById(@PathVariable("id") Long id);

    @GetMapping(value = "/chigo/payment/feign/timeOut")
    String feignTimeOut();
}
