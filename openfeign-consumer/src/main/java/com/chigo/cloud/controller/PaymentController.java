package com.chigo.cloud.controller;

import com.chigo.cloud.entities.CommonResult;
import com.chigo.cloud.service.IPaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @auther cyxu
 * @date 2023-03-25 17:10
 */
@Slf4j
@RestController
@RequestMapping("/payment")
public class PaymentController {

    @Resource
    private IPaymentService paymentService;

    @Value("${server.port}")
    private String serverPort;

    @Resource
    private DiscoveryClient discoveryClient;

    @GetMapping(value = "/findPayment/{id}")
    public CommonResult getPaymentById(@PathVariable("id") Long id) {
        var payment = paymentService.getPaymentById(id);
        log.info("serverPort:{}", serverPort);
        if (null != payment) {
            return new CommonResult(200, "操作成功！serverPort:" + serverPort, payment);
        }
        return new CommonResult(201, "未查询到数据！serverPort:" + serverPort, null);
    }

    @GetMapping("/feign/timeOut")
    public String feignTimeOut() {
        return paymentService.feignTimeOut();
    }
}
